#mkdir -p all_samples
#for myfastq in $(ls ../Sample_*/*.fastq.gz); do ln -s $myfastq . ; done

#snakemake --config RUN=190606_NB501182_0384_AHMLTNAFXY PRJ=BernardiR_983_ChiPSeq --cluster-config cluster.json --cluster "qsub -l select=1:ncpus={cluster.nCPUs}:mem={cluster.memory}Mb:app=java" --jobs 60 --use-conda --rerun-incomplete chunks

#snakemake --config RUN=190412_A00626_0030_AHJMFTDSXX PRJ=PapponeC_955_Brugada_WGS --cluster "qsub -o errors -e errors -N smk.{name}.{jobid}.sh" --jobs 20 --use-conda chunks

#snakemake --config RUN=190412_A00626_0030_AHJMFTDSXX PRJ=PapponeC_955_Brugada_WGS --cluster "qsub -o errors -e errors -N smk.{name}.{jobid}.sh" --jobs 20 --use-conda

configfile: "config.yaml"

#workdir: outdir
#cercare in raw data con il nome della run (Jun_Jul issue)
#proc = subprocess.Popen(["find /lustre1/SampleSheets/ -maxdepth 2 -type d -name " + config['RUN']] + "_IEM.csv", stdout=subprocess.PIPE, shell=True)
#(out, err) = proc.communicate()
#SAMPLESHEET = str(out.strip().decode("utf-8")
#RAW_DIR = str(out.strip().decode("utf-8") + "/Data/Intensities/BaseCalls")
RAW_DIR = "/lustre2/raw_data/" + config['RUN'] + "/Project_" + config['PRJ'] + "/all_samples"
SAMPLES_TMP, = glob_wildcards(RAW_DIR+"/{sample}_R1_001.fastq.gz")
SAMPLES = [x for x in SAMPLES_TMP if not x.startswith('Undetermined')]
#NAMES = [x.split('_')[0] for x in SAMPLES]
NAMES = [x for x in SAMPLES]
#IDS = [x.split('_')[1] for x in SAMPLES]
#LANES = [x.split('_')[2] for x in SAMPLES]
#MYSAMPLES_TMP, = glob_wildcards(config['RUN']+"/BAM/{mysample}_L001.bam")
#MYSAMPLES_TMP, = glob_wildcards(config['RUN']+"/TRIMMED/{mysample}_L001_R1.duk.fq.gz")
MYSAMPLES_TMP, = glob_wildcards(config['RUN']+"/BAM/{mysample}_dedup.bam")
MYSAMPLES = [x for x in MYSAMPLES_TMP]
#MYCHR = ["chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX", "chrY"]
MYCHR = ["chr1", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr2", "chr20", "chr21", "chr22", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chrX", "chrY"]
# a pseudo-rule that collects the target files
rule all:
	input:
		tsv = expand(config['RUN']+"/RESULTS/{name}_filtered.tsv", name=NAMES),
		flagstat_pre = expand(config['RUN']+"/METRICS/{sample}_flagstat_pre.txt", sample=SAMPLES)
# change the input to obtain last file of the pipeline

# a pseudo-rule that collects the target files
rule chunks:
	input:
		flagstat_pre = expand(config['RUN']+"/METRICS/{sample}_flagstat_pre.txt", sample=SAMPLES)

rule fastqc_chunks:
        input:
                R1 = expand(config['RUN']+"/FASTQC/{sample}_R1_001_fastqc.zip", sample=SAMPLES)

rule trim_chunks:
        input:
                R1 = expand(config['RUN']+"/TRIMMED/{sample}_R1.duk.fq.gz", sample=SAMPLES)

#rule trim_chunks:
#        input:
#                R1 = expand(config['RUN']+"/TRIMMED/{sample}_R1.duk.fq.gz", sample=SAMPLES),
#                R2 = expand(config['RUN']+"/TRIMMED/{sample}_R2.duk.fq.gz", sample=SAMPLES)

#issues
rule merge_all:
	input:
		merge = expand(config['RUN']+"/BAM/{mysample}_merged.bam", mysample=MYSAMPLES)

rule markdup_all:
	input:
		markdup = expand(config['RUN']+"/BAM/{mysample}_markdup.bam", mysample=MYSAMPLES)

rule bam_dedup_all:
	input:
		dedup = expand(config['RUN']+"/BAM/{mysample}_dedup.bam", mysample=MYSAMPLES)


rule bw_dedup_all:
	input:
		dedup = expand(config['RUN']+"/BAM/{mysample}_dedup.bw", mysample=MYSAMPLES)

rule peaks_all:
	input:
		peaks = expand(config['RUN']+"/PEAKS/{mysample}_dedup.narrowPeak", mysample=MYSAMPLES)

rule fastqc:
	input:
		R1 = RAW_DIR+"/{S}_{I}_{L}_R1_001.fastq.gz"
	output: 
		R1_zip = config['RUN']+"/FASTQC/{S}_{I}_{L}_R1_001_fastqc.zip"
	params:
		outdir=config['RUN']+"/FASTQC/",
		cmd_cpus="8"
	shell:
		"""
		mkdir -p {params.outdir}
		condactivate; \
		fastqc -o {params.outdir} -t {params.cmd_cpus} {input.R1}
		"""

rule trimming:
	input:
		R1 = RAW_DIR+"/{S}_{I}_{L}_R1_001.fastq.gz"
	output:
		R1 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R1.duk.fq.gz"
	params:
		outdir=config['RUN']+"/TRIMMED/"
	resources:
		mem_mb=15000
	shell:
		"""
		mkdir -p {params.outdir}
		/lustre1/ctgb-usr/local/src/bbmap/bbduk.sh \
		-Xmx14g \
		in={input.R1} \
		out={output.R1} \
		ref=/lustre1/genomes/Illumina_Adapters/BBDUK/adapters.fa \
		k=23 mink=11 rcomp=t ktrim=f kmask=X qtrim=rl trimq=5 \
		forcetrimleft=2 forcetrimright2=2 overwrite=true
		"""

#rule trimming:
#	input:
#		R1 = RAW_DIR+"/{S}_{I}_{L}_R1_001.fastq.gz",
#		R2 = RAW_DIR+"/{S}_{I}_{L}_R2_001.fastq.gz"
#	output:
#		R1 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R1.duk.fq.gz",
#		R2 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R2.duk.fq.gz"
#	params:
#		outdir=config['RUN']+"/TRIMMED/"
#	resources:
#		mem_mb=15000
#	shell:
#		"""
#		mkdir -p {params.outdir}
#		/lustre1/ctgb-usr/local/src/bbmap/bbduk.sh \
#		-Xmx14g \
#		in={input.R1} in2={input.R2} \
#		out={output.R1} out2={output.R2} \
#		ref=/lustre1/genomes/Illumina_Adapters/BBDUK/adapters.fa \
#		k=23 mink=11 rcomp=t ktrim=f kmask=X qtrim=rl trimq=5 \
#		forcetrimleft=2 forcetrimright2=2 overwrite=true
#		"""

rule align:
	input:
		R1 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R1.duk.fq.gz"
	output:
		bam = config['RUN']+"/BAM/{S}_{I}_{L}.bam",
		index = config['RUN']+"/BAM/{S}_{I}_{L}.bam.bai"
	params:
		rg=r"@RG\tID:{I}\tPL:illumina\tPU:{L}\tSM:{S}\tLB:{I}\tCN:CTGB\tSO:coordinate",
		ref_genome=config['ref_genome'],
		samtools=config['SAMTOOLS'],
		bwa=config['BWA'],
		bamdir=config['RUN']+"/BAM/",
		cmd_cpus="8"
	threads: 9
	resources:
		mem_mb=32000
	shell:
		"""
		mkdir -p {params.bamdir}
		{params.bwa} mem -t {params.cmd_cpus} \
		-R '{params.rg}' \
		{params.ref_genome} {input.R1} \
		| {params.samtools} view -Su - \
		| {params.samtools} sort -T {output.bam}.tmp -@ {params.cmd_cpus} - -o {output.bam}
		{params.samtools} index {output.bam}
		"""

#rule align:
#	input:
#		R1 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R1.duk.fq.gz",
#		R2 = config['RUN']+"/TRIMMED/{S}_{I}_{L}_R2.duk.fq.gz"
#	output:
#		bam = config['RUN']+"/BAM/{S}_{I}_{L}.bam",
#		index = config['RUN']+"/BAM/{S}_{I}_{L}.bam.bai"
#	params:
#		rg=r"@RG\tID:{I}\tPL:illumina\tPU:{L}\tSM:{S}\tLB:{I}\tCN:CTGB\tSO:coordinate",
#		ref_genome=config['ref_genome'],
#		samtools=config['SAMTOOLS'],
#		bwa=config['BWA'],
#		bamdir=config['RUN']+"/BAM/",
#		cmd_cpus="8"
#	threads: 9
#	resources:
#		mem_mb=32000
#	shell:
#		"""
#		mkdir -p {params.bamdir}
#		{params.bwa} mem -t {params.cmd_cpus} \
#		-R '{params.rg}' \
#		{params.ref_genome} {input.R1} {input.R2} \
#		| {params.samtools} view -Su - \
#		| {params.samtools} sort -T {output.bam}.tmp -@ {params.cmd_cpus} - -o {output.bam}
#		{params.samtools} index {output.bam}
#		"""

rule flagstat:
	input:
		config['RUN']+"/BAM/{S}_{I}_{L}.bam"
	output:
		config['RUN']+"/METRICS/{S}_{I}_{L}_flagstat_pre.txt"
	params:
		samtools=config['SAMTOOLS'],
		statdir=config['RUN']+"/METRICS/"
	resources:
		mem_mb=2000
	shell:
		"""
		mkdir -p {params.statdir}
		{params.samtools} flagstat {input} > {output}
		"""

rule merge_bams:
	input:
		lane2 = config['RUN']+"/BAM/{S}_{I}_L002.bam",
		lane3 = config['RUN']+"/BAM/{S}_{I}_L003.bam",
		lane4 = config['RUN']+"/BAM/{S}_{I}_L004.bam"
	output:
		config['RUN']+"/BAM/{S}_{I}_merged.bam"
	threads: 4
	resources:
		mem_mb=32000
	shell:
		"""
		picard.jar MergeSamFiles \
			I={input.lane2} \
			I={input.lane3} \
			I={input.lane4} \
			O={output} \
			USE_THREADING=true \
			VALIDATION_STRINGENCY=LENIENT \
			AS=true CREATE_INDEX=true
		"""

#    wrapper:
#        "0.34.0/bio/picard/mergesamfiles"

rule markdup:
	input:
		#index = RAW_DIR+"/{S}_{I}_{L}_I2_001.fastq.gz",
		bam = config['RUN']+"/BAM/{S}_{I}_merged.bam"
	output:
		marked = config['RUN']+"/BAM/{S}_{I}_markdup.bam",
		metrics = config['RUN']+"/BAM/{S}_{I}_markdup_metrics.txt"
	params:
		tempdir=config['tempdir']
	threads:
		8
	resources:
		mem_mb=48000
	shell:
		"""
		export _JAVA_OPTIONS=-Djava.io.tmpdir={params.tempdir} && \
		java -Xmx48g \
		-jar /usr/local/cluster/bin/picard.jar \
		MarkDuplicates \
		I={input.bam} \
		O={output.marked} \
		M={output.metrics} \
		VALIDATION_STRINGENCY=LENIENT \
		ASSUME_SORTED=coordinate \
		CREATE_INDEX=true
		"""

rule dedup_sample:
	input:
		markdup = config['RUN']+"/BAM/{sample}_merged.bam"
	output:
		dedup = config['RUN']+"/BAM/{sample}_dedup.bam",
		bai = config['RUN']+"/BAM/{sample}_dedup.bam.bai",
		metrics = config['RUN']+"/BAM/{sample}_dedup_metrics.txt"
	params:
		ref_genome_fa=config['ref_genome_fa'],
		tempdir=config['tempdir']
	threads:
		8
	resources:
		mem_mb=48000
	shell:
		"""
		rm -f {output.bai}
		export _JAVA_OPTIONS=-Djava.io.tmpdir={params.tempdir} && \
		java -Xmx48g \
		-jar /usr/local/cluster/bin/picard.jar \
		MarkDuplicates \
		I={input.markdup} \
		O={output.dedup} \
		M={output.metrics} \
		VALIDATION_STRINGENCY=LENIENT \
		ASSUME_SORTED=coordinate \
		REMOVE_DUPLICATES=true
		samtools index {output.dedup}
		"""

rule bw_sample:
	input:
		bam = config['RUN']+"/BAM/{sample}_{bamtype}.bam",
	output:
		bw = config['RUN']+"/BAM/{sample}_{bamtype}.bw",
	params:
		ref_genome_fa=config['ref_genome_fa'],
		blacklist=config['blacklist'],
		genome_size_bp=config['genome_size_bp'],
		mapping_qual_bw=15
	threads:
		8
	resources:
		mem_mb=48000
	#conda:
	#	"conda-base.yaml"
	shell:
		"""
		condactivate deeptools
	        bamCoverage -b {input.bam} \
	        --outFileName {output.bw} \
	        --blackListFileName {params.blacklist} \
	        --numberOfProcessors {threads} \
	        --normalizeTo1x {params.genome_size_bp} \
	        --minMappingQuality {params.mapping_qual_bw} \
	        --ignoreDuplicates \
	        --binSize 50
		"""

#filters

rule peak_calling:
	input:
		# distinguere treatment e control
		treatment = config['RUN']+"/BAM/{sample}_dedup.bam",
		control = config['RUN']+"/BAM/"+config['INPUT_NAME']+"_dedup.bam"
	output:
		config['RUN']+"/PEAKS/{sample}_dedup.narrowPeak"
	params:
		macs2=config['MACS2'],
		peaks_dir=config['RUN']+"/PEAKS/",
		gsize=config['genome_size_bp'],
		qvalue=config['peaks_qvalue'],
		outdir = config['RUN']+"/PEAKS/"
	resources:
		mem_mb=24000
	shell:
		"""
		mkdir -p {params.peaks_dir}
		{params.macs2} callpeak \
		--treatment {input.treatment} \
		--control {input.control} \
		--name {wildcards.sample} \
		--outdir {params.outdir} \
		--gsize {params.gsize} \
		--bdg \
		--qvalue {params.qvalue}
		"""
